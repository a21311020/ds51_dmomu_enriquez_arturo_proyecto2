import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

class Temp extends StatefulWidget {
  const Temp({super.key, required this.title});

  final String title;

  @override
  State<Temp> createState() => _TempState();
}

class _TempState extends State<Temp> {
  String dropdownValue = 'Celsius';
  double celsius = 0;
  double fahrenheit = 32;
  double kelvin = 273.15;
  String v1 = '';
  String v2 = '';
  OpValue opValue = OpValue();

  TextEditingController _controller = TextEditingController();
  TextEditingController _controller2 = TextEditingController();

  @override
  Widget build(BuildContext context) {
    List listTemp = ['Celsius', 'Fahrenheit', 'Kelvin'];

    calcul2() {
      if (opValue.v1 == 'Celsius') {
        if (opValue.v2 == 'Fahrenheit') {
          double result = double.parse(_controller.text) + fahrenheit;
          _controller2.text = result.toString();
        } else if (opValue.v2 == 'Kelvin') {
          double result = double.parse(_controller.text) + kelvin;
          _controller2.text = result.toString();
        } else if (opValue.v2 == 'Celsius') {
          double result = double.parse(_controller.text);
          _controller2.text = result.toString();
        }
      } else if (opValue.v1 == 'Fahrenheit') {
        if (opValue.v2 == 'Celsius') {
          double result = double.parse(_controller.text) * 1.8 + 32;
          _controller2.text = result.toString();
        } else if (opValue.v2 == 'Kelvin') {
          double result = double.parse(_controller.text);
          result = (-32) * 5 / 9 + 275.15;
          _controller2.text = result.toString();
        } else if (opValue.v2 == 'Fahrenheit') {
          double result = double.parse(_controller.text);
          _controller2.text = result.toString();
        }
      } else if (opValue.v1 == 'Kelvin') {
        if (opValue.v2 == 'Celsius') {
          double result = double.parse(_controller.text) - 273.15;
          _controller2.text = result.toString();
        } else if (opValue.v2 == 'Fahrenheit') {
          double result = double.parse(_controller.text) * 9 / 5 - 459.67;
          _controller2.text = result.toString();
        } else if (opValue.v2 == 'Kelvin') {
          double result = double.parse(_controller.text);
          _controller2.text = result.toString();
        }
      }
    }

    void _onButtonPressed(String buttonText) {
      if (buttonText == '<') {
        _controller.text =
            _controller.text.substring(0, _controller.text.length - 1);
      } else if (buttonText == 'C') {
        calcul2();
      } else if (buttonText == '.') {
        if (!_controller.text.contains('.')) {
          _controller.text += buttonText;
        }
      } else if (buttonText == '=') {
        var expression = _controller.text;
        var parser = Parser();
        var parsedExpression = parser.parse(expression);
        var context = ContextModel();
        var result = parsedExpression.evaluate(EvaluationType.REAL, context);
        _controller.text = result.toString();

        // historial.add(_controller.text);
        // _historial.text = historial[cont];
        // cont += 1;
      } else {
        String currentValue = _controller.text;
        _controller.text = '$currentValue$buttonText';
      }

      setState(() {});
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            const UserAccountsDrawerHeader(
              accountName: Text('The BEST Calculadora'),
              accountEmail: Text(''),
              currentAccountPicture: CircleAvatar(
                child: ClipOval(
                  child: Image(
                    image: AssetImage('aaa.jpeg'),
                    fit: BoxFit.cover,
                    width: double.infinity,
                    height: double.infinity,
                  ),
                ),
              ),
            ),
            ListTile(
              leading: const Icon(Icons.calculate_rounded),
              title: Text('Calculadora'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/calc');
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text(
                        'Bienvenido a la calculadora más Perrona, DEBERAS!'),
                    duration: Duration(seconds: 2),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.straighten_rounded),
              title: Text('Sistema Métrico'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/sism');
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text(
                        'Aqui convertimos a lo que sea lo que te mida, wapo ;)'),
                    duration: Duration(seconds: 2),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      body: Column(
        children: [
          Expanded(
            flex: 1,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                  child: Text(
                    _controller.text,
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 25),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 10, 220, 10),
                  child: DropdownButtonFormField(
                    items: listTemp.map((temp) {
                      return DropdownMenuItem(
                        child: Text(temp, style: TextStyle(fontSize: 26.0)),
                        value: temp,
                      );
                    }).toList(),
                    onChanged: (value) {
                      opValue.optenevalue(value);
                    },
                    itemHeight: 50.0,
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 20, 10, 0),
                  child: Text(
                    _controller2.text,
                    textAlign: TextAlign.right,
                    style: TextStyle(fontSize: 25),
                  ),
                ),
                Container(
                  margin: const EdgeInsets.fromLTRB(10, 10, 220, 10),
                  child: DropdownButtonFormField(
                    items: listTemp.map((temp) {
                      return DropdownMenuItem(
                        child: Text(temp, style: TextStyle(fontSize: 26.0)),
                        value: temp,
                      );
                    }).toList(),
                    onChanged: (value2) {
                      opValue.optenevalue1(value2);
                    },
                    itemHeight: 50.0,
                  ),
                ),
                Container(
                    margin: const EdgeInsets.fromLTRB(10, 0, 10, 0),
                    child: Column(
                      children: [],
                    )),
              ],
            ),
          ),
          Expanded(
              flex: 1,
              child: Column(
                children: [
                  //Fila 1
                  Expanded(
                    flex: 1,
                    child: Row(
                      children: [
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 0, 0, 0),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 0, 0, 0),
                            child: CustomButton(
                              text: '=',
                              onPressed: () {
                                _onButtonPressed('C');
                              },
                              borderRadius: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 0, 7, 0),
                            child: CustomIconButton(
                              onPressed: () {
                                _onButtonPressed('<');
                              },
                              borderRadius: 20,
                              icon: Icons.backspace,
                              iconSize: 30.0,
                              color: Colors.red,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // Fila 2
                  Expanded(
                    flex: 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 0, 0),
                            child: CustomButton(
                              text: '7',
                              onPressed: () {
                                _onButtonPressed('7');
                              },
                              borderRadius: 20,
                              color: const Color.fromRGBO(124, 65, 158, 1),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 0, 0),
                            child: CustomButton(
                              text: '8',
                              onPressed: () {
                                _onButtonPressed('8');
                              },
                              borderRadius: 20,
                              color: const Color.fromRGBO(124, 65, 158, 1),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 7, 0),
                            child: CustomButton(
                              text: '9',
                              onPressed: () {
                                _onButtonPressed('9');
                              },
                              borderRadius: 20,
                              color: const Color.fromRGBO(124, 65, 158, 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // Fila 3
                  Expanded(
                    flex: 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 0, 0),
                            child: CustomButton(
                              text: '4',
                              onPressed: () {
                                _onButtonPressed('4');
                              },
                              borderRadius: 20,
                              color: const Color.fromRGBO(124, 65, 158, 1),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 0, 0),
                            child: CustomButton(
                              text: '5',
                              onPressed: () {
                                _onButtonPressed('5');
                              },
                              borderRadius: 20,
                              color: const Color.fromRGBO(124, 65, 158, 1),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 7, 0),
                            child: CustomButton(
                              text: '6',
                              onPressed: () {
                                _onButtonPressed('6');
                              },
                              borderRadius: 20,
                              color: const Color.fromRGBO(124, 65, 158, 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // Fila 4
                  Expanded(
                    flex: 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 0, 0),
                            child: CustomButton(
                              text: '1',
                              onPressed: () {
                                _onButtonPressed('1');
                              },
                              borderRadius: 20,
                              color: const Color.fromRGBO(124, 65, 158, 1),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 0, 0),
                            child: CustomButton(
                              text: '2',
                              onPressed: () {
                                _onButtonPressed('2');
                              },
                              borderRadius: 20,
                              color: const Color.fromRGBO(124, 65, 158, 1),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 7, 0),
                            child: CustomButton(
                              text: '3',
                              onPressed: () {
                                _onButtonPressed('3');
                              },
                              borderRadius: 20,
                              color: const Color.fromRGBO(124, 65, 158, 1),
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                  // Fila 5
                  Expanded(
                    flex: 1,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 0, 7),
                            child: CustomButton(
                              text: '+/-',
                              onPressed: () {},
                              borderRadius: 20,
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 0, 7),
                            child: CustomButton(
                              text: '0',
                              onPressed: () {
                                _onButtonPressed('0');
                              },
                              borderRadius: 20,
                              color: const Color.fromRGBO(124, 65, 158, 1),
                            ),
                          ),
                        ),
                        Expanded(
                          child: Container(
                            margin: const EdgeInsets.fromLTRB(7, 7, 7, 7),
                            child: CustomButton(
                              text: '.',
                              onPressed: () {},
                              borderRadius: 20,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ],
              ))
        ],
      ),
    );
  }
}

class CustomButton extends StatelessWidget {
  final String? text;
  final Color color;
  final double borderRadius;
  final VoidCallback onPressed;

  const CustomButton({
    this.text,
    required this.onPressed,
    this.color = Colors.blueGrey,
    this.borderRadius = 10,
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 90,
      height: 90,
      child: ElevatedButton(
        onPressed: onPressed,
        child: text != null
            ? Text(
                text!,
                style: TextStyle(fontSize: 40),
              )
            : SizedBox(),
        style: ElevatedButton.styleFrom(
          primary: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius),
          ),
        ),
      ),
    );
  }
}

class CustomIconButton extends StatelessWidget {
  final Color color;
  final double borderRadius;
  final VoidCallback onPressed;
  final IconData? icon; // se actualiza para permitir que el valor sea opcional
  final double iconSize;

  const CustomIconButton({
    required this.onPressed,
    this.color = Colors.blueGrey,
    this.borderRadius = 10,
    this.icon, // se actualiza para requerir el valor del usuario
    this.iconSize = 24.0, // establecer un valor por defecto de 24.0
  });

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 90,
      height: 90,
      child: ElevatedButton.icon(
        onPressed: onPressed,
        icon: Icon(icon,
            size:
                iconSize), // se utiliza el valor proporcionado por el usuario o un SizedBox() vacío si no se proporciona un icono// se utiliza el valor proporcionado por el usuario
        label: SizedBox(),
        style: ElevatedButton.styleFrom(
          primary: color,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(borderRadius),
          ),
        ),
      ),
    );
  }
}

class OpValue {
  String v1 = '';
  String v2 = '';

  void optenevalue(value1) {
    this.v1 = value1;
  }

  void optenevalue1(value2) {
    this.v2 = value2;
  }
}

// void calcul() {
//   print(this.v1 + this.v2);
//   if (this.v1 == 'Celsius' && this.v2 == 'Fahrenheit') {
//     double celsius = 25.0; // ejemplo de valor para v1
//     double fahrenheit =
//         (celsius * 9 / 5) + 32; // realiza el cálculo de conversión
//     print(fahrenheit); // imprime el resultado de la conversión
//   }
// }
