import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:math_expressions/math_expressions.dart';

class Calc extends StatefulWidget {
  const Calc({super.key, required this.title});

  final String title;

  @override
  State<Calc> createState() => _CalcState();
}

class _CalcState extends State<Calc> {
  TextEditingController _controller = TextEditingController();
  TextEditingController _historial = TextEditingController();

  int cont = 0;
  // List<String> historial = [];

  @override
  Widget build(BuildContext context) {
    void _onButtonPressed(String buttonText) {
      if (buttonText == '<') {
        _controller.text =
            _controller.text.substring(0, _controller.text.length - 1);
      } else if (buttonText == 'C') {
        _controller.text = '';
      } else if (buttonText == '.') {
        if (!_controller.text.contains('.')) {
          _controller.text += buttonText;
        }
      } else if (buttonText == '=') {
        var expression = _controller.text;
        var parser = Parser();
        var parsedExpression = parser.parse(expression);
        var context = ContextModel();
        var result = parsedExpression.evaluate(EvaluationType.REAL, context);
        _controller.text = result.toString();

        // historial.add(_controller.text);
        // _historial.text = historial[cont];
        // cont += 1;
      } else {
        String currentValue = _controller.text;
        _controller.text = '$currentValue$buttonText';
      }

      setState(() {});
    }

    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
        flexibleSpace: Container(
          decoration: const BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Colors.purple,
                Color.fromARGB(255, 49, 0, 69),
              ],
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
            ),
          ),
        ),
      ),
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            const UserAccountsDrawerHeader(
              accountName: Text('Eskeler'),
              accountEmail: Text(''),
              currentAccountPicture: CircleAvatar(
                child: ClipOval(
                  child: Image(
                    image: AssetImage('gengar.jpg'),
                    fit: BoxFit.cover,
                    width: double.infinity,
                    height: double.infinity,
                  ),
                ),
              ),
            ),
            ListTile(
              leading: const Icon(Icons.calculate_rounded),
              title: Text('Calculadora'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/calc');
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('Calculadoreichon papu'),
                    duration: Duration(seconds: 3),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.straighten_rounded),
              title: Text('Sistema Métrico'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/sism');
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text(
                        'Aqui convertimos a lo que sea lo que te mida, wapo ;)'),
                    duration: Duration(seconds: 2),
                  ),
                );
              },
            ),
            ListTile(
              leading: Icon(Icons.thermostat_rounded),
              title: Text('Temperatura'),
              onTap: () {
                Navigator.pop(context);
                Navigator.pushNamed(context, '/temp');
                ScaffoldMessenger.of(context).showSnackBar(
                  const SnackBar(
                    content: Text('Bueno, bueno, pero que candente, wapo ;)'),
                    duration: Duration(seconds: 2),
                  ),
                );
              },
            ),
          ],
        ),
      ),
      body: Container(
        child: Column(
          children: [
            Expanded(
                flex: 3,
                child: Container(
                    child: Column(
                  children: [
                    Expanded(
                        flex: 3,
                        child: Container(
                          alignment: Alignment.bottomRight,
                          color: Color.fromARGB(255, 185, 155, 66),
                          child: Text(
                            _controller.text,
                            textAlign: TextAlign.right,
                            style: TextStyle(fontSize: 25),
                          ),
                        )),
                    Expanded(
                        flex: 3,
                        child: Container(
                          alignment: Alignment.bottomRight,
                          color: Colors.amber,
                          child: Text(
                            _historial.text,
                            textAlign: TextAlign.right,
                            style: TextStyle(fontSize: 25),
                          ),
                        ))
                  ],
                ))),
            Expanded(
                flex: 1,
                child: Container(
                  color: Colors.blue,
                  // child: Text(_result.text),
                )),
            Expanded(
                flex: 6,
                child: Container(
                  child: Row(
                    children: [
                      Expanded(
                        flex: 4,
                        child: Container(
                          color: Colors.amber,
                          child: Column(
                            children: [
                              MiButton(
                                contenidoText: 'C',
                                onPressed: () {
                                  _onButtonPressed('C');
                                },
                              ),
                              MiButton(
                                contenidoText: '7',
                                onPressed: () {
                                  _onButtonPressed('7');
                                },
                              ),
                              MiButton(
                                contenidoText: '4',
                                onPressed: () {
                                  _onButtonPressed('4');
                                },
                              ),
                              MiButton(
                                contenidoText: '1',
                                onPressed: () {
                                  _onButtonPressed('1');
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Container(
                          color: Colors.amber,
                          child: Column(
                            children: [
                              MiButton(
                                contenidoText: '<',
                                onPressed: () {
                                  _onButtonPressed('<');
                                },
                              ),
                              MiButton(
                                contenidoText: '8',
                                onPressed: () {
                                  _onButtonPressed('8');
                                },
                              ),
                              MiButton(
                                contenidoText: '5',
                                onPressed: () {
                                  _onButtonPressed('5');
                                },
                              ),
                              MiButton(
                                contenidoText: '2',
                                onPressed: () {
                                  _onButtonPressed('2');
                                },
                              ),
                              MiButton(
                                contenidoText: '0',
                                onPressed: () {
                                  _onButtonPressed('0');
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Container(
                          color: Colors.amber,
                          child: Column(
                            children: [
                              MiButton(
                                contenidoText: '%',
                                onPressed: () {
                                  _onButtonPressed('%');
                                },
                              ),
                              MiButton(
                                contenidoText: '9',
                                onPressed: () {
                                  _onButtonPressed('9');
                                },
                              ),
                              MiButton(
                                contenidoText: '6',
                                onPressed: () {
                                  _onButtonPressed('6');
                                },
                              ),
                              MiButton(
                                contenidoText: '3',
                                onPressed: () {
                                  _onButtonPressed('3');
                                },
                              ),
                              MiButton(
                                contenidoText: '.',
                                onPressed: () {
                                  _onButtonPressed('.');
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        flex: 4,
                        child: Container(
                          color: Colors.amber,
                          child: Column(
                            children: [
                              MiButton(
                                contenidoText: '/',
                                onPressed: () {
                                  _onButtonPressed('/');
                                },
                              ),
                              MiButton(
                                contenidoText: '*',
                                onPressed: () {
                                  _onButtonPressed('*');
                                },
                              ),
                              MiButton(
                                contenidoText: '-',
                                onPressed: () {
                                  _onButtonPressed('-');
                                },
                              ),
                              MiButton(
                                contenidoText: '+',
                                onPressed: () {
                                  _onButtonPressed('+');
                                },
                              ),
                              MiButton(
                                contenidoText: '=',
                                onPressed: () {
                                  _onButtonPressed('=');
                                },
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                )),
          ],
        ),
      ),
    );
  }
}

class MiButton extends StatelessWidget {
  final String contenidoText;
  final VoidCallback onPressed;
  MiButton({required this.contenidoText, required this.onPressed});
  @override
  Widget build(BuildContext context) {
    return Expanded(
        // Usa los parámetros para personalizar el widget
        flex: 5,
        child: Container(
          color: Colors.white,
          child: MaterialButton(
            minWidth: 200.0,
            height: 40.0,
            onPressed: onPressed,
            color: Colors.lightBlue,
            child: Text(this.contenidoText,
                style: TextStyle(color: Colors.white, fontSize: 30)),
          ),
        ));
  }
}

String sumar(
  String a,
) {
  String resultado = '';
  resultado += a;
  return resultado;
}

// mainAxisAlignment: MainAxisAlignment.center,
//         children: <Widget>[
//           const Text(
//             'You have pushed the button this many times:',
//           ),

//         ],
// floatingActionButton: FloatingActionButton(
//   onPressed: _incrementCounter,
//   tooltip: 'Increment',
//   child: const Icon(Icons.add),

// int _counter = 0;
// void _incrementCounter() {
//   setState(() {
//     _counter++;
//   });
// // }
// class FibonacciScreen extends StatefulWidget {
//   @override
//   _FibonacciScreenState createState() => _FibonacciScreenState();
// }


// class _FibonacciScreenState extends State<FibonacciScreen> {
//   int _n = 0;
//   List<int> _fibonacci = [];

//   void _calculateFibonacci() {
//     int a = 0;
//     int b = 1;

//     _fibonacci.clear();

//     for (int i = 0; i < _n; i++) {
//       _fibonacci.add(a);
//       int sum = a + b;
//       a = b;
//       b = sum;
//     }

//     setState(() {});
//   }




// class Area {
//   double a = 0;
//   double b = 0;
//   Area(double a, double b) {
//     this.a = a;
//     this.b = b;
//   }
//   double multArea() {
//     return this.a * this.b;
//   }

//   double div() {
//     return this.a / this.b;
//   }
// }

// var ola = Area(13, 2).div();
