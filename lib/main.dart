import 'package:flutter/material.dart';
import 'home.dart';
import 'temp.dart';
import 'sisM.dart';
import 'masa.dart';
import 'capa.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.green,
        brightness: Brightness.light,
      ),
      darkTheme: ThemeData(
        brightness: Brightness.dark,
      ),
      themeMode: ThemeMode.system,
      home: const Calc(title: 'Calculadora'),
      routes: {
        '/calc': (context) => const Calc(title: "Calculadora"),
        '/temp': (context) => const Temp(title: "Temperatura"),
        '/sism': (context) => const Sism(title: "Sistema Metrico"),
        '/capa': (context) => const Capa(title: "Capacidad"),
        '/masa': (context) => const Masa(title: "Masa"),
      },
    );
  }
}
